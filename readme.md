## Step 1: Install packages

add package in composer.json
```json
"require-dev": {
	"way/generators": "~2.0"
}
```

Run in the Terminal: `composer update --dev`

Open `app/config/app.php` and add a new item to the providers array:

```
'Way\Generators\GeneratorsServiceProvider'
```

If you want, we can change the file path's

```bash
php artisan generate:publish-templates
```

Now, open `app/config/packages/way/generators/config.php`

```php
// Change
'view_target_path'   => app_path('views')
// To
'view_target_path'   => app_path('views/admin')
```

Next you should open `app/templates/scaffolding/controller.txt` and change the `Views`

```php
return View::make('$COLLECTION$.index', compact('$COLLECTION$'));
// Became
return View::make('admin.$COLLECTION$.index', compact('$COLLECTION$'));
```

## Step 2: Basic configuration

#### Edit Application Evironment:

File: `bootstrap/start.php`

```php
	'local' => array('machine-name')
```

#### Database Connection

Configure your database in: `app/config/local/database.php`

## Step 3: Scaffoldings

1. Restaurants:

```bash
php artisan generate:scaffold restaurants --fields="name:string, address:string, city:string, about:text, phone:string, delivery:string"
```

2. Cuisines:

```bash
php artisan generate:scaffold cuisines --fields="name:string"
```

3. Products:

```bash
php artisan generate:scaffold products --fields="name:string, price:float, description:text, restaurant_id:integer, category_id:integer"
```

4. Categories

```bash
php artisan generate:scaffold categories --fields="name:string, restaurant_id:integer"
```

5. Pivot table (Restaurant Cuisine)

```bash
php artisan generate:pivot cuisines restaurants
php artisan migrate
```

## Models

File: `app/models/Restaurant.php`

```php
protected $fillable = ['name', 'address', 'city', 'about', 'phone', 'delivery'];

public static $rules = [
	'name'    => 'required',
	'address' => 'required',
	'city'    => 'required',
	'about'   => 'required'
];

public function cuisines()
{
	return $this->belongsToMany('Cuisine', 'cuisine_restaurant', 'restaurant_id', 'cuisine_id');
}

public function products()
{
	return $this->hasMany('Product');
}

public function categories()
{
	return $this->hasMany('Category');
}
```

File: `app/models/Cuisine.php`

```php
protected $fillable = ['name'];

public static $rules = ['name' => 'required'];

public function restaurants()
{
	return $this->belongsToMany('Restaurant', 'cuisine_restaurant', 'cuisine_id', 'restaurant_id');
}
```

File: `app/models/Product.php`

```php
protected $fillable = ['name', 'price', 'description', 'restaurant_id', 'category_id'];

public static $rules = [
	'name' => 'required',
	'restaurant_id' => 'required'
];

public function restaurant()
{
	return $this->belongsTo('Restaurant');
}

public function category()
{
	return $this->belongsTo('Category');
}
```

File: `app/models/Category.php`

```php
protected $fillable = ['name', 'restaurant_id'];

public static $rules = ['name' => 'required'];

public function restaurant()
{
	return $this->belongsTo('Restaurant');
}

public function products()
{
	return $this->hasMany('Product');
}
```

## Seeds

This is actually dropped, im not going to seeding

File: `app/database/seeds/RestaurantsTableSeeder.php`

```php
```

File: `app/database/seeds/CuisinesTableSeeder.php`

```php
```

Make pivot seed

```bash
php artisan generate:seed cuisine_restaurant
```

File: `app/database/seeds/CuisineRestaurantTableSeeder.php`

```php
```

File: `app/database/seeds/CategoriesTableSeeder.php`

```php
```

File: `app/database/seeds/ProductsTableSeeder.php`

```php
```

File: `app/database/seeds/DatabaseSeeder.php`

```php
$this->call('RestaurantsTableSeeder');
$this->call('CuisinesTableSeeder');
$this->call('CuisineRestaurantTableSeeder');
$this->call('CategoriesTableSeeder');
$this->call('ProductsTableSeeder');
```

## Routes

```php
Route::resource('admin/restaurants', 'RestaurantsController');
Route::resource('admin/cuisines', 'CuisinesController');
Route::resource('admin/products', 'ProductsController');
Route::resource('admin/categories', 'CategoriesController');
```

## Controllers

## Idea
Restaurant.wifi

## Colors?
BG: #E4DDC1
#1EA59F
#99D5CB
#F1CE2C
#43362D

## Good UI References
http://mailchimp.com/
http://bundlin.com/13851
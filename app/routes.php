<?php

/** ------------------------------------------
 *  Route model binding
 *  ------------------------------------------
 */
Route::model('restaurant', 'Restaurant');

/** ------------------------------------------
 *  Admin Routes
 *  ------------------------------------------
 */
Route::group(['prefix' => 'admin', "before" => "auth"], function()
{
	# Restaurants Management
	Route::resource('restaurants', 'RestaurantsController');

	# Cuisines Management
	Route::resource('cuisines', 'CuisinesController');

	# Categories Management
	Route::get('categories/create/{restaurant}', 'CategoriesController@create');
	Route::post('categories/store/{restaurant}', 'CategoriesController@store');
	Route::resource('categories', 'CategoriesController');

	# Products Management
	Route::get('products/create/{restaurant}', 'ProductsController@create');
	Route::post('products/store/{restaurant}', 'ProductsController@store');
	Route::resource('products', 'ProductsController');

	# Login Management
	Route::any('users/profile', ['uses' => 'UsersController@profile',
		'as' => 'admin.users.profile']);
	Route::any('users/logout', ['uses' => 'UsersController@logout',
		'as' => 'admin.users.logout']);
});


/** ------------------------------------------
 *  Frontend Routes
 *  ------------------------------------------
 */

# Login backend
Route::any('admin/login', ['uses' => 'UsersController@login',
	'as' => 'admin.login']);

# Restaurants
Route::get('restaurant/{restaurantSlug}', 'HomeController@restaurantView');
Route::get('restaurants', 'HomeController@restaurantIndex');

# Cuisines
Route::get('cuisine/{cuisineSlug}', 'HomeController@cuisineView');
Route::get('cuisines', 'HomeController@cuisineIndex');

# Search
Route::get('api/search', 'ApiSearchController@index');

# Index Page - Last route, no matches
Route::get('/', 'HomeController@index');
@extends('layouts.default')

{{-- Web site Title --}}
@section('title')
Cuisines Title
@stop

{{-- Content --}}
@section('content')

	<!-- Page Content -->
	<div class="container">

		<!-- Page Heading -->
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Cuisines
					<small>Secondary Text</small>
				</h1>
			</div>
		</div>
		<!-- /.row -->

		<div class="row">

			<!-- Content Column -->
			<div class="col-lg-8">

				<!-- Project One -->
				@foreach($cuisines as $cuisine)
				<div class="media">
					<div class="media-body">
						<h4 class="media-heading"><a href="{{ URL::action('HomeController@cuisineView', $cuisine->slug) }}">{{ $cuisine->name }}</a></h4>
					</div>
				</div>
				@endforeach

			</div>

			<!-- Sidebar Widgets Column -->
			<div class="col-md-4">

				<!-- Search Well -->
				<div class="well">
					<h4>Search</h4>
					<select id="searchbox" name="q" placeholder="Search field..." class="form-control center-block"></select>
				</div>

				<!-- Cuisines Well -->
				<div class="well">
					<h4>Something here</h4>
					<!-- /.row -->
				</div>

			</div>

		</div>
		<!-- /.row -->

@stop
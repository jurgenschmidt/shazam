@extends('layouts.default')

{{-- Web site Title --}}
@section('title')
Homepage Title
@stop

{{-- Google Map Canvas --}}
@section('styles')

<style type="text/css">
#map-canvas {
	margin: 10px 7%;
	height: 360px;
}
.grid {
	position: relative;
	clear: both;
	margin: 0 auto;
	padding: 1em 0 4em;
	max-width: 1000px;
	list-style: none;
	text-align: center;
}
.grid figure {
	position: relative;
	float: left;
	overflow: hidden;
	margin: 10px 1%;
	min-width: 320px;
	max-width: 480px;
	max-height: 360px;
	width: 48%;
	height: auto;
	background: #3085a3;
	text-align: center;
	cursor: pointer;
}
.grid figure img {
	position: relative;
	display: block;
	min-height: 100%;
	max-width: 100%;
	opacity: 0.8;
}
.grid figure figcaption {
	padding: 2em;
	color: #fff;
	text-transform: uppercase;
	font-size: 1.25em;
	-webkit-backface-visibility: hidden;
	backface-visibility: hidden;
}
.grid figure figcaption::before,
.grid figure figcaption::after {
	pointer-events: none;
}
.grid figure figcaption,
.grid figure figcaption > a {
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
}
/* Anchor will cover the whole item by default */
.grid figure figcaption > a {
	z-index: 1000;
	text-indent: 200%;
	white-space: nowrap;
	font-size: 0;
	opacity: 0;
}
.grid figure h2 {
	font-weight: 300;
}
.grid figure h2 span {
	font-weight: 800;
}
.grid figure h2,
.grid figure p {
	margin: 0;
}
.grid figure p {
	font-size: 68.5%;
}
/*-----------------*/
/***** Steve *****/
/*-----------------*/
figure.effect-steve {
	z-index: auto;
	overflow: visible;
	background: #000;
}
figure.effect-steve:before,
figure.effect-steve h2:before {
	position: absolute;
	top: 0;
	left: 0;
	z-index: -1;
	width: 100%;
	height: 100%;
	background: #000;
	content: '';
	-webkit-transition: opacity 0.35s;
	transition: opacity 0.35s;
}
figure.effect-steve:before {
	box-shadow: 0 3px 30px rgba(0,0,0,0.8);
	opacity: 0;
}
figure.effect-steve figcaption {
	z-index: 1;
}
figure.effect-steve img {
	opacity: 1;
	-webkit-transition: -webkit-transform 0.35s;
	transition: transform 0.35s;
	-webkit-transform: perspective(1000px) translate3d(0,0,0);
	transform: perspective(1000px) translate3d(0,0,0);
}
figure.effect-steve h2,
figure.effect-steve p {
	background: #fff;
	color: #2d434e;
}
figure.effect-steve h2 {
	position: relative;
	margin-top: 2em;
	padding: 0.25em;
}
figure.effect-steve h2:before {
	box-shadow: 0 1px 10px rgba(0,0,0,0.5);
}
figure.effect-steve p {
	margin-top: 1em;
	padding: 0.5em;
	font-weight: 800;
	opacity: 0;
	-webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
	transition: opacity 0.35s, transform 0.35s;
	-webkit-transform: scale3d(0.9,0.9,1);
	transform: scale3d(0.9,0.9,1);
}
figure.effect-steve:hover:before {
	opacity: 1;
}
figure.effect-steve:hover img {
	-webkit-transform: perspective(1000px) translate3d(0,0,21px);
	transform: perspective(1000px) translate3d(0,0,21px);
}
figure.effect-steve:hover h2:before {
	opacity: 0;
}
figure.effect-steve:hover p {
	opacity: 1;
	-webkit-transform: scale3d(1,1,1);
	transform: scale3d(1,1,1);
}
</style>
@stop

{{-- Content --}}
@section('content')
<!-- Page Content -->
<div class="container">

	<h1 class="text-center">Busca</h1>

	<div class="row">
		<div class="col-lg-4 col-lg-offset-4">
			<select id="searchbox" name="q" placeholder="Digite sua busca..." class="form-control center-block"></select>
		</div>
	</div>

	<div class="row">
		<h3 class="text-center">Novos Restaurantes</h3>

		<div class="grid">
		@foreach ($restaurants as $restaurant)
			<figure class="effect-steve">
				<img src="{{ asset('img/restaurant').'/'.$restaurant->slug }}" alt="" />
				<figcaption>
					<h2>{{ $restaurant->name }}<!--  <span>Steve</span> --></h2>
					<p><span class="glyphicon glyphicon-map-marker"></span> {{ $restaurant->address }}, {{ $restaurant->city }}</p>
					<a href="{{ URL::action('HomeController@restaurantView', $restaurant->slug) }}">View more</a>
				</figcaption>
			</figure>
		@endforeach
		</div>
	</div>

	<div id="map-canvas"></div>

</div>
@stop

@section('scripts')
<script type="text/javascript">
var locations = [
	['Bondi Beach',    -24.558756, -54.04644, 4],
	['Coogee Beach',   -24.568856, -54.06511, 5],
	['Cronulla Beach', -24.551000, -54.06644, 3],
	['Manly Beach',    -24.549999, -54.05644, 2],
	['Maroubra Beach', -24.558756, -54.05144, 1]
];

var map = new google.maps.Map(document.getElementById('map-canvas'), {
	zoom: 14,
	center: new google.maps.LatLng(-24.558756, -54.05644),
	mapTypeId: google.maps.MapTypeId.ROADMAP
});

var infowindow = new google.maps.InfoWindow();

var marker, i;

for (i = 0; i < locations.length; i++) {
	marker = new google.maps.Marker({
		position: new google.maps.LatLng(locations[i][1], locations[i][2]),
		map: map
	});

	google.maps.event.addListener(marker, 'click', (function(marker, i) {
		return function() {
			infowindow.setContent(locations[i][0]);
			infowindow.open(map, marker);
		}
	})(marker, i));
}
</script>
@stop
@extends('layouts.default')

{{-- Web site Title --}}
@section('title')
Restaurants Title
@stop

{{-- Content --}}
@section('content')

	<!-- Page Content -->
	<div class="container">

		<!-- Page Heading -->
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Restaurants</h1>
			</div>
		</div>
		<!-- /.row -->

		<div class="row">

			<!-- Content Column -->
			<div class="col-lg-8">

				<!-- Project One -->
				@foreach($restaurants as $restaurant)
				<div class="media">
					<a href="#" class="pull-left">
						<img class="media-object" src="http://placehold.it/64x64" alt="">
					</a>
					<div class="media-body">
						<h4 class="media-heading"><a href="{{ URL::action('HomeController@restaurantView', $restaurant->slug) }}">{{ $restaurant->name }}</a></h4>
						<p><span class="glyphicon glyphicon-map-marker"></span> {{ $restaurant->address }}, {{ $restaurant->city }} <span class="glyphicon glyphicon-earphone"></span> {{ $restaurant->phone }}</p>
					</div>
				</div>
				@endforeach

			</div>

			<!-- Sidebar Widgets Column -->
			<div class="col-md-4">

				<!-- Search Well -->
				<div class="well">
					<h4>Search</h4>
					<select id="searchbox" name="q" placeholder="Search field..." class="form-control center-block"></select>
				</div>

				<!-- Cuisines Well -->
				<div class="well">
					<h4>Cuisines</h4>
					<div class="row">
						<div class="col-lg-6">
							<ul class="list-unstyled">
								<li><a href="#">Cuisine Name</a>
								</li>
								<li><a href="#">Cuisine Name</a>
								</li>
								<li><a href="#">Cuisine Name</a>
								</li>
								<li><a href="#">Cuisine Name</a>
								</li>
							</ul>
						</div>
						<div class="col-lg-6">
							<ul class="list-unstyled">
								<li><a href="#">Cuisine Name</a>
								</li>
								<li><a href="#">Cuisine Name</a>
								</li>
								<li><a href="#">Cuisine Name</a>
								</li>
								<li><a href="#">Cuisine Name</a>
								</li>
							</ul>
						</div>
					</div>
					<!-- /.row -->
				</div>

			</div>

		</div>
		<!-- /.row -->

@stop
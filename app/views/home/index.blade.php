@extends('layouts.default')

@section('styles')
<style type="text/css">
@import url(http://fonts.googleapis.com/css?family=Kreon:400,700);

html,
body {
	height: 100%;
	/* The html and body elements cannot have any padding or margin. */
	-webkit-font-smoothing: antialiased;
	font: normal 14px arial, sans-serif;
	position: relative;
}
.row {
	margin-left: 0px;
	margin-right: 0px;
}
/* Wrapper for page content to push down footer */
#wrap {
	min-height: 100%;
	height: auto !important;
	/* Negative indent footer by its height */
	margin: 0 auto -60px;
	/* Pad bottom by footer height */
	padding: 0 0 60px;
}
/* Fixed height of the footer */
#footer {
	height: 60px;
	background-color: #f5f5f5;
	margin-top: 50px;
	padding-top: 20px;
	padding-bottom: 20px;
}
/* Custom page CSS
-------------------------------------------------- */
#wrap > .container {
	padding: 80px 15px 0;
}
.container .credit {
	margin: 20px 0;
}
#footer {
	background-color: #414141;
}
#footer a {
	color: #efefef;
}
h1,
h2,
h3,
h4 {
	font-family: 'Kreon', serif;
	vertical-align: middle;
}
header {
	background: #f16251;
}
header h1,
header a,
header a:hover {
	color: #efefef;
	font-weight: 800;
	text-decoration: none;
}
h1 {
	font-size: 50px;
}
h2 {
	font-size: 40px;
}
#nav {
	width: 100%;
	position: static;
	top: -32px;
}
#nav.affix {
	position: fixed;
	top: 0;
	z-index: 10;
	-webkit-transition: all .6s ease-in-out;
}
@media (min-width: 767px) {
	.navbar-nav.nav-justified > li {
		float: none;
	}
}
.navbar-nav {
	margin: 1px;
}
/* customize nav style */
.navbar-custom {
	background-color: #2e2e2e;
	font-weight: 700;
	text-transform: uppercase;
	border-width: 0;
}
.navbar-custom .navbar-nav > li > a {
	color: #ddd;
}
.navbar-custom .navbar-nav li > a:hover,
.navbar-nav li .open,
.navbar-custom .navbar-nav .active a {
	background-color: #000;
}
.navbar-collapse.in { /* 3.0.2 bug workaround */
	overflow-y: visible;
}
.navbar-toggle {
	outline: 0;
}
.panel {
	border-width: 0;
}
@media (max-width: 768px) {
	header {
		height: 95px;
	}
}
#map-canvas {
	width: 100%;
	height: 300px;
	margin: 0;
	padding: 15px;
}
.scroll-top {
	bottom: 0;
	position: fixed;
	right: 6%;
	z-index: 100;
	background: #ffcc33;
	font-size: 24px;
	border-top-left-radius: 3px;
	border-top-right-radius: 3px;
}
.scroll-top a:link,
.scroll-top a:visited {
	color: #222;
}
section {
	min-height: 90%;
	width: 100%;
}
.bg-1 {
	background: url('http://www.bootply.com/assets/example/bg_4.jpg') no-repeat center center fixed;
	-webkit-background-size: cover;
	-moz-background-size: cover;
	-o-background-size: cover;
	background-size: cover;
}
.bg-2 {
	background: url('http://www.bootply.com/assets/example/bg_5.jpg') no-repeat center center fixed;
	-webkit-background-size: cover;
	-moz-background-size: cover;
	-o-background-size: cover;
	background-size: cover;
}
.bg-3 {
	background: url('http://www.bootply.com/assets/example/bg_6.jpg') no-repeat center center fixed;
	-webkit-background-size: cover;
	-moz-background-size: cover;
	-o-background-size: cover;
	background-size: cover;
}
.bg-4 {
	padding-top:30px;
	background-image: -webkit-gradient(linear, left top, left bottom, from(rgba(20,20,20,0.2)),to(rgba(255,255,255,0)), color-stop(1,#000));
}

/*--- Custom Yuga ---*/
#destaque {
	height: 400px;
	background: url('https://d262ilb51hltx0.cloudfront.net/max/1400/1*YpFQOi1XHSUQmhjqbU2qXw.png') no-repeat;
	-webkit-background-size: cover;
	-moz-background-size: cover;
	-o-background-size: cover;
	background-size: cover;
	background-position: center;
}
section > h2 {
	padding: 20px;
	
}

</style>
@stop

{{-- Content --}}
@section('content')
<!-- Wrap all page content here -->
<div id="wrap">

<header class="masthead">
	<div class="container">
	<div class="row">
		<div class="col-sm-6">
		<h1><a href="#" title="Brand name">Brand name</a>
			<p class="lead">Slogan goes here</p></h1>
		</div>
	</div>
	</div>
</header>

<!-- Navbar -->
<div class="navbar navbar-custom navbar-inverse navbar-static-top" id="nav">
	<div class="container">
		<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		</div>
		<div class="collapse navbar-collapse">
		<ul class="nav navbar-nav nav-justified">
			<li><a href="#home">Home</a></li>
			<li><a href="#destaque">Destaque</a></li>
			<li><a href="#section4">Section4</a></li>
			<li><a href="#section5">Section5</a></li>
		</ul>
		</div>
	</div>
</div>













<!-- Section Home -->
<section id="home">
<div class="container">
	<div class="col-sm-10 col-sm-offset-1">
	<div class="page-header text-center">
		<h2>Sticky Footer with Fly-in Navbar</h2>
	</div>
	<p class="lead text-center">
		Twitter Bootstrap is a front-end toolkit to rapidly build web applications.
	</p>
	<div class="col-sm-6 col-sm-offset-3" style="margin-top: 40px;">
		<select id="searchbox" name="q" placeholder="Digite sua busca..." class="form-control center-block"></select>
	</div>
	</div>
</div>
</section>











<!-- Section 2 -->
<section id="destaque">
	<div class="col-sm-6 col-sm-offset-3 text-center">
		<h2>Destaque</h2>
	</div>
</section>

<div class="bg-4">
<div class="row">
	@foreach ($restaurants as $restaurant)
	<div class="col-sm-4 col-xs-6">
	<div class="panel panel-default">

		<div><img src="{{ asset('img/restaurant').'/'.$restaurant->slug }}" class="img-responsive"/></div>

		<div class="panel-body">
		<p class="lead">{{ $restaurant->name }}</p>
		<p><span class="glyphicon glyphicon-map-marker"></span> {{ $restaurant->address }}, {{ $restaurant->city }}</p>
		<a href="{{ URL::action('HomeController@restaurantView', $restaurant->slug) }}">View more</a>
		</div>
	</div><!--/panel-->
	</div><!--/col-->
	@endforeach

</div><!--/row-->
</div>











<!-- Section 4 -->
<section class="bg-3">
	<div class="col-sm-6 col-sm-offset-3 text-center"><h2 style="padding:20px;background-color:rgba(5,5,5,.8)">Section4</h2></div>
</section>

<div class="bg-4"></div><!--/container-->












<!-- Section 5 -->
<div class="row">

	<h2 class="text-center">Section5</h2>

	<div id="map-canvas"></div>

	<hr>

	<div class="col-sm-8">

		<div class="row form-group">
		<div class="col-xs-4">
			<input type="text" class="form-control" id="firstName" name="firstName" placeholder="First Name" required="">
		</div>
		<div class="col-xs-4">
			<input type="text" class="form-control" id="middleName" name="firstName" placeholder="Middle Name" required="">
		</div>
		<div class="col-xs-4">
			<input type="text" class="form-control" id="lastName" name="lastName" placeholder="Last Name" required="">
		</div>
		</div>
		<div class="row form-group">
			<div class="col-xs-6">
			<input type="email" class="form-control" name="email" placeholder="Email" required="">
			</div>
			<div class="col-xs-6">
			<input type="email" class="form-control" name="phone" placeholder="Phone" required="">
			</div>
		</div>
		<div class="row form-group">
			<div class="col-xs-12">
			<input type="homepage" class="form-control" placeholder="Website URL" required="">
			</div>
		</div>
		<div class="row form-group">
			<div class="col-xs-12">
			<button class="btn btn-default pull-right">Contact Us</button>
			</div>
		</div>

	</div>
	<div class="col-sm-4 pull-right">
	<div class="well">
		<h4>Cuisines</h4>
		<div class="row">
			<div class="col-lg-6">
				<ul class="list-unstyled">
					<li><a href="#">Cuisine Name</a>
					</li>
					<li><a href="#">Cuisine Name</a>
					</li>
					<li><a href="#">Cuisine Name</a>
					</li>
					<li><a href="#">Cuisine Name</a>
					</li>
				</ul>
			</div>
			<div class="col-lg-6">
				<ul class="list-unstyled">
					<li><a href="#">Cuisine Name</a>
					</li>
					<li><a href="#">Cuisine Name</a>
					</li>
					<li><a href="#">Cuisine Name</a>
					</li>
					<li><a href="#">Cuisine Name</a>
					</li>
				</ul>
			</div>
		</div>
		<!-- /.row -->
	</div>
	</div>

</div><!--/row-->

<div class="container">
	<div class="col-sm-8 col-sm-offset-2 text-center">
		<h2>Beautiful Bootstrap Templates</h2>

		<hr>
		<h4>
		We love templates. We love Bootstrap.
		</h4>
		<p>Get more free templates like this at the <a href="http://bootply.com">Bootstrap Playground</a>, Bootply.</p>
		<hr>

	</div><!--/col-->
</div><!--/container-->

</div><!--/wrap-->















<div id="footer">
	<div class="container">
	<p class="text-muted">This Bootstrap Example courtesy <a href="http://www.bootply.com">Bootply.com</a></p>
	</div>
</div>

<ul class="nav pull-right scroll-top">
	<li><a href="#" title="Scroll to top"><i class="glyphicon glyphicon-chevron-up"></i></a></li>
</ul>

@stop

@section('scripts')
<script type="text/javascript">
/* affix the navbar after scroll below header */
$('#nav').affix({
		offset: {
			top: $('header').height()-$('#nav').height()
		}
});

/* smooth scrolling for scroll to top */
$('.scroll-top').click(function(){
	$('body,html').animate({scrollTop:0},1000);
})

/* smooth scrolling for nav sections */
$("#nav ul li a[href^='#']").on('click', function(e) {
	e.preventDefault();

	var hash = this.hash;

	$('html, body').animate({
		scrollTop: $(this.hash).offset().top
	}, 300, function(){
		window.location.hash = hash;
	});
});

/* google maps */

// enable the visual refresh
google.maps.visualRefresh = true;

var map;
function initialize() {
	var mapOptions = {
	zoom: 15,
	scrollwheel: false,
	mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	map = new google.maps.Map(document.getElementById('map-canvas'),
		mapOptions);
	// try HTML5 geolocation
	if(navigator.geolocation) {
	navigator.geolocation.getCurrentPosition(function(position) {
		var pos = new google.maps.LatLng(position.coords.latitude,
										 position.coords.longitude);

		var infowindow = new google.maps.InfoWindow({
		map: map,
		position: pos,
		content: 'Location found using HTML5.'
		});

		map.setCenter(pos);
	}, function() {
		handleNoGeolocation(true);
	});
	} else {
	// browser doesn't support geolocation
	handleNoGeolocation(false);
	}
}

function handleNoGeolocation(errorFlag) {
	if (errorFlag) {
		var content = 'Error: The Geolocation service failed.';
	} else {
		var content = 'Error: Your browser doesn\'t support geolocation.';
	}

	var options = {
		map: map,
		position: new google.maps.LatLng(-24.558756, -54.05644),
		content: content
	};

	var infowindow = new google.maps.InfoWindow(options);
	map.setCenter(options.position);
}
google.maps.event.addDomListener(window, 'load', initialize);

</script>
@stop
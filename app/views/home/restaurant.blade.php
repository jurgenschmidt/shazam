@extends('layouts.default')

{{-- Web site Title --}}
@section('title')
{{ $restaurant->name }}
@stop

@section('styles')
<style type="text/css">
.header-banner {
	background: #191918;
	color: #fff;
	display: table;
	min-height: 70vh;
	margin-bottom: 50px;
	width: 100%;
}
.header-section {
	display: block;
	width: 100%;
	position: absolute;
	top: 50px;
}
.section-background-image {
	background-size: cover;
	background-position: center;
	height: 70vh;
	opacity: 0.7;
}
.header-title {
	position: absolute;
	top: 50%;
	left: 50%;
	margin-right: -50%;
	transform: translate(-50%, -50%);
	padding-bottom: 160px;
	letter-spacing: -0.02em;
	font-weight: 700;
	font-style: normal;
	font-size: 60px;
	line-height: 1;
	letter-spacing: -0.04em;
}
#map-canvas {
	height: 300px;
	width: 100%;
}
</style>
@stop

{{-- Content --}}
@section('content')

<!-- Full Width Image Header with Logo -->
<div class="header-banner">
	<div class="header-section">
		<div class="section-background-image" style="background-image: url({{ asset('img/restaurant').'/'.$restaurant->slug }});"></div>
	</div>
	<h1 class="header-title">{{ $restaurant->name }}</h1>
</div>

<!-- Page Content -->
<div class="container">

	<div class="row">

		<!-- Products Column -->
		<div class="col-lg-8">

			@foreach ($categories as $category)
			<div class="panel panel-primary">
				<!-- Default panel contents -->
				<div class="panel-heading"><h3 class="panel-title">{{ $category->name }}</h4></div>
				<!-- List group -->
				<ul class="list-group">
				@foreach ($category->products()->get() as $product)
					<li class="list-group-item">
						<span class="badge">R$ {{ $product->price }}</span>
						<strong>{{ $product->name }}</strong><br>{{ $product->description }}
					</li>
				@endforeach
				</ul>
			</div>
			@endforeach

		</div>

		<!-- Sidebar Widgets Column -->
		<div class="col-md-4">

			<!-- Search Well -->
			<div class="well">
				<h4>Search</h4>
				<select id="searchbox" name="q" placeholder="Search field..." class="form-control center-block"></select>
			</div>

			<!-- Cuisines Well -->
			<div class="well">
				<h4>Informações</h4>
				<ul class="list-unstyled">
					<li><span class="glyphicon glyphicon-map-marker"></span> {{ $restaurant->address}}, {{ $restaurant->city }}</li>
					<li><span class="glyphicon glyphicon-earphone"></span> {{ $restaurant->phone }}</li>
					<li><span class="glyphicon glyphicon-bell"></span> {{ $restaurant->delivery }}</li>
				</ul>
				<hr/>
				<h4>Sobre</h4>
				<p>{{ $restaurant->about }}</p>
			</div>

		</div>
		<!-- /.side-bar -->

	</div>
	<!-- /.row -->
</div>

<div id="map-canvas"></div>

@stop


@section('scripts')
<script type="text/javascript">

	google.maps.event.addDomListener(window, 'load', init);
	var map;
	function init() {
		var mapOptions = {
			center: new google.maps.LatLng(-24.558756,-54.05644),
			zoom: 14,
			zoomControl: true,
			zoomControlOptions: {
				style: google.maps.ZoomControlStyle.SMALL,
			},
			disableDoubleClickZoom: true,
			mapTypeControl: false,
			scaleControl: false,
			scrollwheel: true,
			panControl: false,
			streetViewControl: true,
			draggable : true,
			overviewMapControl: false,
			overviewMapControlOptions: {
				opened: false,
			},
			mapTypeId: google.maps.MapTypeId.ROADMAP,
		}
		var mapElement = document.getElementById('map-canvas');
		var map = new google.maps.Map(mapElement, mapOptions);
		marker = new google.maps.Marker({
			position: new google.maps.LatLng(-24.558756, -54.05644),
			map: map
		});
	}

</script>
@stop
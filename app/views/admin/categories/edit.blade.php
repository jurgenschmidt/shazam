<h1>Editar Categoria</h1>

{{ Form::open( [ 'route' => ['admin.categories.update', $category->id], 'method' => 'PUT' ] ) }}

{{ Form::label('name', 'Nome da Categoria') }}
{{ Form::text('name', $category->name) }}
<br>
<button type="submit" class="btn btn-success">Editar</button>

{{ Form::close() }}
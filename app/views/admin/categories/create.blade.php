<h1>Criar categoria</h1>
Restaurante: <strong>{{ $restaurant->name }}</strong>

{{ Form::open( [ 'action' => ['CategoriesController@store', $restaurant->id] ] ) }}

{{ Form::label('name', 'Nome da Categoria') }}
{{ Form::text('name', Input::old('name')) }}

<button type="submit" class="btn btn-success">Cadastrar</button>

{{ Form::close() }}
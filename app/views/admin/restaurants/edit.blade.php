@extends('layouts.admin')

{{-- Web site Title --}}
@section('title')
Editar Restaurante :: @parent
@stop

{{-- Content --}}
@section('content')
<h3>Editar Restaurante</h3>

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

<div class="well col-lg-12">
	{{ Form::open( [ 'route' => ['admin.restaurants.update', $restaurant->id], 'class' => 'form-horizontal', 'method' => 'PUT', 'files' => true ] ) }}
		<div class="form-group">
			{{ Form::label('name', 'Nome do restaurante', array('class'=>'col-md-2 control-label')) }}
			<div class="col-sm-10">
				{{ Form::text('name', $restaurant->name, array('class'=>'form-control', 'placeholder'=>'Nome do restaurante')) }}
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('address', 'Endereço', array('class'=>'col-md-2 control-label')) }}
			<div class="col-sm-10">
				{{ Form::text('address', $restaurant->address, array('class'=>'form-control', 'placeholder'=>'Endereço')) }}
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('city', 'Cidade', array('class'=>'col-md-2 control-label')) }}
			<div class="col-sm-10">
				{{ Form::text('city', $restaurant->city, array('class'=>'form-control', 'placeholder'=>'Cidade')) }}
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('about', 'Sobre o restaurante', array('class'=>'col-md-2 control-label')) }}
			<div class="col-sm-10">
				{{ Form::textarea('about', $restaurant->about, array('class'=>'form-control', 'rows'=>'3', 'placeholder'=>'Sobre o restaurante')) }}
				<span class="help-block">Descreva um pouco sobre.</span>
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('phone', 'Telefone', array('class'=>'col-md-2 control-label')) }}
			<div class="col-sm-10">
				{{ Form::text('phone', $restaurant->phone, array('class'=>'form-control', 'placeholder'=>'Telefone')) }}
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('delivery', 'Entrega', array('class'=>'col-md-2 control-label')) }}
			<div class="col-sm-10">
				{{ Form::text('delivery', $restaurant->delivery, array('class'=>'form-control', 'placeholder'=>'Entrega')) }}
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('latitude', 'Latitude', array('class'=>'col-md-2 control-label')) }}
			<div class="col-sm-10">
				{{ Form::text('latitude', $restaurant->latitude, array('class'=>'form-control', 'placeholder'=>'Latitude')) }}
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('longitude', 'Longitude', array('class'=>'col-md-2 control-label')) }}
			<div class="col-sm-10">
				{{ Form::text('longitude', $restaurant->longitude, array('class'=>'form-control', 'placeholder'=>'Longitude')) }}
			</div>
		</div>

		<div class="form-group">
			<label for="cuisines" class="col-lg-2 control-label">Cuisines</label>
			<div class="col-lg-10">
				{{ Form::select('cuisines[]', $cuisines, $restaurantCuisines, ['multiple', 'class' => 'form-control']) }}
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('image', 'Imagem', array('class'=>'col-md-2 control-label')) }}
			<div class="col-sm-10">
				{{ Form::file('image', Input::old('image'), array('class'=>'form-control', 'placeholder'=>'Imagem')) }}
			</div>
		</div>

		<div class="form-group">
			<div class="col-lg-10 col-lg-offset-2">
				<button type="submit" class="btn btn-success">Editar</button>
			</div>
		</div>

{{ Form::close() }}

</div>

@stop
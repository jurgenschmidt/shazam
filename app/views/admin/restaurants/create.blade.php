@extends('layouts.admin')

{{-- Web site Title --}}
@section('title')
Criar Restaurante :: @parent
@stop

{{-- Content --}}
@section('content')
<h3>Criar Restaurante</h3>

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

<div class="well col-lg-12">
	{{ Form::open( [ 'route' => 'admin.restaurants.store', 'class' => 'form-horizontal', 'files' => true ] ) }}
		<div class="form-group">
			{{ Form::label('name', 'Nome do restaurante', array('class'=>'col-md-2 control-label')) }}
			<div class="col-sm-10">
				{{ Form::text('name', Input::old('name'), array('class'=>'form-control', 'placeholder'=>'Nome do restaurante')) }}
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('address', 'Endereço', array('class'=>'col-md-2 control-label')) }}
			<div class="col-sm-10">
				{{ Form::text('address', Input::old('address'), array('class'=>'form-control', 'placeholder'=>'Endereço')) }}
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('city', 'Cidade', array('class'=>'col-md-2 control-label')) }}
			<div class="col-sm-10">
				{{ Form::text('city', Input::old('city'), array('class'=>'form-control', 'placeholder'=>'Cidade')) }}
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('about', 'Sobre o restaurante', array('class'=>'col-md-2 control-label')) }}
			<div class="col-sm-10">
				{{ Form::textarea('about', Input::old('about'), array('class'=>'form-control', 'rows'=>'3', 'placeholder'=>'Sobre o restaurante')) }}
				<span class="help-block">Descreva um pouco sobre.</span>
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('phone', 'Telefone', array('class'=>'col-md-2 control-label')) }}
			<div class="col-sm-10">
				{{ Form::text('phone', Input::old('phone'), array('class'=>'form-control', 'placeholder'=>'Telefone')) }}
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('delivery', 'Entrega', array('class'=>'col-md-2 control-label')) }}
			<div class="col-sm-10">
				{{ Form::text('delivery', Input::old('delivery'), array('class'=>'form-control', 'placeholder'=>'Entrega')) }}
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('latitude', 'Latitude', array('class'=>'col-md-2 control-label')) }}
			<div class="col-sm-10">
				{{ Form::text('latitude', Input::old('latitude'), array('class'=>'form-control', 'placeholder'=>'Latitude')) }}
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('longitude', 'Longitude', array('class'=>'col-md-2 control-label')) }}
			<div class="col-sm-10">
				{{ Form::text('longitude', Input::old('longitude'), array('class'=>'form-control', 'placeholder'=>'Longitude')) }}
			</div>
		</div>

		<div class="form-group">
			<label for="cuisines" class="col-lg-2 control-label">Cuisines</label>
			<div class="col-lg-10">
				<select multiple="" class="form-control" name="cuisines[]" id="cuisines">
				@foreach ( $cuisines as $cuisine )
					<option value="{{ $cuisine->id }}">{{ $cuisine->name }}</option>
				@endforeach
				</select>
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('image', 'Imagem', array('class'=>'col-md-2 control-label')) }}
			<div class="col-sm-10">
				{{ Form::file('image', Input::old('image'), array('class'=>'form-control', 'placeholder'=>'Imagem')) }}
			</div>
		</div>

		<div class="form-group">
			<div class="col-lg-10 col-lg-offset-2">
				<button type="submit" class="btn btn-success">Cadastrar</button>
			</div>
		</div>

{{ Form::close() }}

</div>

@stop
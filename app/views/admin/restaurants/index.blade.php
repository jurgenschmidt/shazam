@extends('layouts.admin')

{{-- Web site Title --}}
@section('title')
Restaurants :: @parent
@stop

{{-- Content --}}
@section('content')

<div class="page-header">
	<h3>Restaurants</h3>
	<a href="{{{ URL::to('admin/restaurants/create') }}}" class="btn btn-info"><span class="icon-material-add"></span> Novo Restaurante</a>
</div>

<div class="well">

	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-1">#</th>
				<th class="col-md-4">Restaurante</th>
				<th class="col-md-2">Última atualização</th>
				<th class="col-md-2">Ações</th>
			</tr>
		</thead>
		<tbody>
		@foreach ( $restaurants as $restaurant )
			<tr>
				<td>{{ $restaurant->id }}</td>
				<td><a href="{{ URL::action('RestaurantsController@show', $restaurant->id) }}">{{ $restaurant->name }}</a></td>
				<td>{{ $restaurant->updated_at }}</td>
				<td>
					<a href="{{ URL::action('RestaurantsController@edit', $restaurant->id) }}"><span class="label label-primary">Editar</span></a>
					{{ Form::open(['route' => ['admin.restaurants.destroy', $restaurant->id], 'method' => 'DELETE']) }}
						{{ Form::submit('Deletar', ['class' => 'btn btn-warning btn-xs']) }}
					{{ Form::close() }}
				</td>
			</tr>
		@endforeach
		</tbody>
	</table>

</div>

@stop
@extends('layouts.admin')

{{-- Web site Title --}}
@section('title')
Edit {{ $restaurant->name }} :: @parent
@stop

{{-- Content --}}
@section('content')

<div class="page-header">
	<h3>{{ $restaurant->name }}</h3>
</div>

<div class="col-lg-6">
	<div class="well">

		<h4>Produtos</h4>
		<a href="{{ URL::action('ProductsController@create', $restaurant->id) }}" class="btn btn-info btn-xs"><span class="icon-material-add"></span> Criar produto</a>

		<div class="list-group">
		@foreach ($products as $product)
			<div class="list-group-item">
				<div class="row-picture">
					<img class="circle" src="http://lorempixel.com/56/56/people/1" alt="icon">
				</div>
				<div class="row-content">
					<h4 class="list-group-item-heading">
						<a href="{{ URL::action('ProductsController@edit', $product->id) }}">{{ $product->name }}</a>
					</h4>
					<p class="list-group-item-text">
						{{ Form::open(['route' => ['admin.products.destroy', $product->id], 'method' => 'DELETE']) }}
							{{ Form::submit('Deletar') }}
						{{ Form::close() }}
					</p>
				</div>
			</div>
			<div class="list-group-separator"></div>
		@endforeach
		</div>
	</div>
</div>

<div class="col-lg-6">
	<div class="well">
		<h4>Categorias</h4>
		<a href="{{ URL::action('CategoriesController@create', $restaurant->id) }}" class="btn btn-info btn-xs"><span class="icon-material-add"></span> Criar categoria</a>
		<div class="list-group">
		@foreach ($categories as $category)
			<div class="list-group-item">
				<div class="row-picture">
					<img class="circle" src="http://lorempixel.com/56/56/people/2" alt="icon">
				</div>
				<div class="row-content">
					<h4 class="list-group-item-heading">
						<a href="{{ URL::action('CategoriesController@edit', $category->id) }}">{{ $category->name }}</a>
					</h4>
					<p class="list-group-item-text">
						{{ Form::open(['route' => ['admin.categories.destroy', $category->id], 'method' => 'DELETE']) }}
							{{ Form::submit('Deletar') }}
						{{ Form::close() }}
					</p>
				</div>
			</div>
			<div class="list-group-separator"></div>
		@endforeach
		</div>
	</div>
</div>


@stop
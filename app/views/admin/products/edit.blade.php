<h1>Editar produto</h1>

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

{{ Form::open( [ 'route' => ['admin.products.update', $product->id], 'method' => 'PUT' ] ) }}

{{ Form::label('name', 'Nome do Produto') }}
{{ Form::text('name', $product->name) }}
<br>
{{ Form::label('price', 'Preço') }}
{{ Form::text('price', $product->price) }}
<br>
{{ Form::label('description', 'Descrição') }}
{{ Form::text('description', $product->description) }}
<br>
{{ Form::label('category', 'Categoria') }}
{{ Form::select('category_id', $categories, $product->category_id) }}
<br>
<button type="submit" class="btn btn-success">Editar</button>

{{ Form::close() }}
<h1>Criar produto</h1>
Restaurante: <strong>{{ $restaurant->name }}</strong>

{{ Form::open( [ 'action' => ['ProductsController@store', $restaurant->id] ] ) }}

{{ Form::label('name', 'Nome do Produto') }}
{{ Form::text('name', Input::old('name')) }}
<br>
{{ Form::label('price', 'Preço') }}
{{ Form::text('price', Input::old('price')) }}
<br>
{{ Form::label('description', 'Descrição') }}
{{ Form::text('description', Input::old('description')) }}
<br>
{{ Form::label('category', 'Categoria') }}
{{ Form::select('category_id', $categories,'') }}
<br>
<button type="submit" class="btn btn-success">Cadastrar</button>

{{ Form::close() }}
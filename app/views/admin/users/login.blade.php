@extends('layouts.admin')

{{-- Web site Title --}}
@section('title')
Login :: @parent
@stop

@section('content')

<div class="wrapper">
<!-- <form class="form-signin" method="post"> -->
{{ Form::open() }}
<h2 class="form-signin-heading">Please login</h2>
	{{ Form::text("username", Input::old("username"), ['class' => 'form-control', 'placeholder' => 'Username', 'required', 'autofocus']) }}
	{{ Form::password("password", ['class' => 'form-control', 'placeholder' => 'Password', 'required']) }}
{{ Form::submit("login", ['class' => 'btn btn-lg btn-primary btn-block']) }}
{{ Form::close() }}
</div>

{{ $errors->first("password") }}

@stop
@extends('layouts.admin')

{{-- Web site Title --}}
@section('title')
Criar Cuisine :: @parent
@stop

{{-- Content --}}
@section('content')
<h3>Criar Cuisine</h3>

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

<div class="well col-lg-12">
	{{ Form::open( [ 'route' => 'admin.cuisines.store', 'class' => 'form-horizontal' ] ) }}

		<div class="form-group">
			{{ Form::label('name', 'Nome da cuisine', array('class'=>'col-md-2 control-label')) }}
			<div class="col-sm-10">
				{{ Form::text('name', Input::old('name'), array('class'=>'form-control', 'placeholder'=>'Nome da cuisine')) }}
			</div>
		</div>

		<div class="form-group">
			<div class="col-lg-10 col-lg-offset-2">
				<button type="submit" class="btn btn-success">Cadastrar</button>
			</div>
		</div>

{{ Form::close() }}

</div>

@stop
@extends('layouts.admin')

{{-- Web site Title --}}
@section('title')
Cuisines :: @parent
@stop

{{-- Content --}}
@section('content')
	<div class="page-header">
		<h3>Cuisines</h3>

		<!-- <div class="pull-right"> -->
			<a href="{{{ URL::to('admin/cuisines/create') }}}" class="btn btn-info"><span class="icon-material-add"></span> Nova Cuisine</a>
		<!-- </div> -->
	</div>

<div class="well">

	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-1">#</th>
				<th class="col-md-4">Cuisine</th>
				<th class="col-md-2">Última atualização</th>
				<th class="col-md-2">Ações</th>
			</tr>
		</thead>
		<tbody>
		@foreach ( $cuisines as $cuisine )
			<tr>
				<td>{{ $cuisine->id }}</td>
				<td>{{ $cuisine->name }}</td>
				<td>{{ $cuisine->updated_at }}</td>
				<td>
					<a href="{{ URL::action('CuisinesController@edit', $cuisine->id) }}"><span class="label label-primary">Editar</span></a>
					{{ Form::open(['route' => ['admin.cuisines.destroy', $cuisine->id], 'method' => 'DELETE']) }}
						{{ Form::submit('Deletar', ['class' => 'btn btn-warning btn-xs']) }}
					{{ Form::close() }}
				</td>
			</tr>
		@endforeach
		</tbody>
	</table>

</div>

@stop
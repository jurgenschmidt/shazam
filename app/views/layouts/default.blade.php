<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />

	<title>
		@yield('title')
	</title>

	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=&sensor=false&extension=.js"></script>

	<!-- CSS -->
	<link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{asset('bootstrap/css/ripples.min.css')}}">
	<link rel="stylesheet" href="{{asset('bootstrap/css/selectize.bootstrap3.css')}}">
	<link rel="stylesheet" href="{{asset('bootstrap/css/styles.css')}}">
	<!-- <style type="text/css">body { margin-top: 50px; }</style> -->
	@yield('styles')
</head>

<body data-spy="scroll" data-target="#nav">
	<!-- Navbar -->
	<!--
	<li{{ (Request::is('restaurants') ? ' class="active"' : '') }}><a href="{{{ URL::to('restaurants') }}}"><span class="glyphicon glyphicon-cutlery"></span> Restaurants</a></li>
	<li{{ (Request::is('cuisines') ? ' class="active"' : '') }}><a href="{{{ URL::to('cuisines') }}}"><span class="glyphicon glyphicon-tags"></span> Cuisine</a></li>
	-->

	@yield('content')

	<!-- TODO: Footer -->

	<!-- Javascripts -->
	<script type="text/javascript">
		var root = '{{url("/")}}';
	</script>
	<script src="{{asset('bootstrap/js/jquery-2.1.1.min.js')}}"></script>
	<script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
	<script src="{{asset('bootstrap/js/standalone/selectize.min.js') }}"></script>
	<script src="{{asset('bootstrap/js/main.js') }}"></script>

	@yield('scripts')

</body>
</html>
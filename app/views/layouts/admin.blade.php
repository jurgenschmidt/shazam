<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">

	<title>
		@section('title')
			Administration
		@show
	</title>

	<!-- CSS -->
	<link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{asset('bootstrap/css/ripples.min.css')}}">
	<link rel="stylesheet" href="{{asset('bootstrap/css/material-wfont.min.css')}}">

	@yield('styles')
</head>

<body>
	<!-- Container -->
	<div class="container">

		<!-- Navbar -->
		<nav class="navbar navbar-default" role="navigation">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">Brand</a>
			</div>

			<div class="container-fluid">
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

					<ul class="nav navbar-nav">
						<li{{ (Request::is('admin/restaurants*') ? ' class="active"' : '') }}><a href="{{{ URL::to('admin/restaurants') }}}">Restaurants</a></li>
						<li{{ (Request::is('admin/cuisines*') ? ' class="active"' : '') }}><a href="{{{ URL::to('admin/cuisines') }}}">Cuisines</a></li>
					</ul>

					<ul class="nav navbar-nav navbar-right">
					@if (Auth::check())
						<li><a href="{{ URL::route('admin.users.logout') }}">Logout</a></li>
						<li{{ (Request::is('admin/users*') ? ' class="active"' : '') }}><a href="{{ URL::route('admin.users.profile') }}">Profile</a></li>
					@endif
					</ul>

				</div><!-- /.navbar-collapse -->
			</div><!-- /.container-fluid -->
		</nav>

		<!-- ./ navbar -->

		<!-- Content -->
		@yield('content')
		<!-- ./ content -->

		<!-- Footer -->
		<footer class="clearfix">
			@yield('footer')
		</footer>
		<!-- ./ Footer -->

	</div>
	<!-- ./ container -->

	<!-- Javascripts -->
	<script src="{{asset('bootstrap/js/jquery-2.1.1.min.js')}}"></script>
	<script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
	<script src="{{asset('bootstrap/js/ripples.min.js')}}"></script>
	<script src="{{asset('bootstrap/js/material.min.js')}}"></script>

	@yield('scripts')

</body>
</html>
<?php

class UsersController extends \BaseController {

	public function login()
	{
		if ($this->isPostRequest()) {
			$validator = Validator::make($data = Input::only('username', 'password'), User::$rules);

			if ($validator->passes()) {
				if (Auth::attempt($data)) {
					return Redirect::route("admin.users.profile");
				}

				return Redirect::back()->withErrors([
					"password" => ["Credentials invalid."]
				]);
			} else {
				return Redirect::back()
					->withInput()
					->withErrors($validator);
			}
		}

		return View::make('admin.users.login');
	}

	protected function isPostRequest()
	{
		return Input::server("REQUEST_METHOD") == "POST";
	}

	public function profile()
	{
		return View::make('admin.users.profile');
	}

	public function logout()
	{
		Auth::logout();

		return Redirect::route('admin.login');
	}

}
<?php

class RestaurantsController extends \BaseController {

	/**
	 * Display a listing of restaurants
	 *
	 * @return Response
	 */
	public function index()
	{
		$restaurants = Restaurant::all();

		return View::make('admin.restaurants.index', compact('restaurants'));
	}

	/**
	 * Show the form for creating a new restaurant
	 *
	 * @return Response
	 */
	public function create()
	{
		$cuisines = Cuisine::all();

		return View::make('admin.restaurants.create', compact('cuisines'));
	}

	/**
	 * Store a newly created restaurant in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Restaurant::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$data['slug'] = Str::slug(Input::get('name'));

		if (Input::hasFile('image'))
			Input::file('image')->move(public_path().'/img/restaurant', $data['slug']);

		$restaurant = Restaurant::create($data);
		$restaurant->cuisines()->attach(Input::get('cuisines'));

		return Redirect::route('admin.restaurants.index');
	}

	/**
	 * Display the specified restaurant.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$restaurant = Restaurant::findOrFail($id);
		$products = $restaurant->products()->get();
		$categories = $restaurant->categories()->get();

		return View::make('admin.restaurants.show', compact('restaurant', 'products', 'categories'));
	}

	/**
	 * Show the form for editing the specified restaurant.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$restaurant = Restaurant::find($id);

		foreach ( Cuisine::all() as $c )
			$cuisines[$c['id']] = $c['name'];

		foreach ($restaurant->cuisines()->get() as $k)
			$restaurantCuisines[] = $k['id'];

		return View::make('admin.restaurants.edit', compact('restaurant', 'cuisines', 'restaurantCuisines'));
	}

	/**
	 * Update the specified restaurant in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$restaurant = Restaurant::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Restaurant::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$data['slug'] = Str::slug(Input::get('name'));

		if (Input::hasFile('image'))
			Input::file('image')->move(public_path().'/img/restaurant', $data['slug']);

		$restaurant->update($data);
		$restaurant->cuisines()->sync(Input::get('cuisines'));

		return Redirect::route('admin.restaurants.index');
	}

	/**
	 * Remove the specified restaurant from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Restaurant::destroy($id);

		return Redirect::route('admin.restaurants.index');
	}

}

<?php

/**
 * Api/SearchController is used for the "smart" search throughout the site.
 * it returns and array of items (with type and icon specified) so that the selectize.js plugin
 * can render the search results properly
 **/

class ApiSearchController extends \BaseController
{

	public function appendValue($data, $type, $element)
	{
		// operate on the item passed by reference, adding the element and type
		foreach ($data as $key => & $item) {
			$item[$element] = $type;
		}
		return $data;
	}

	public function appendURL($data, $prefix)
	{
		// operate on the item passed by reference, adding the url based on slug
		foreach ($data as $key => & $item) {
			$item['url'] = url($prefix.'/'.$item['slug']);
		}
		return $data;
	}

	public function index()
	{
		// Retrieve the user's input and escape it
		$query = e(Input::get('q',''));

		// If the input is empty, return an error response
		if (!$query && $query == '') return Response::json([], 400);

		$restaurants = Restaurant::where('name','like','%'.$query.'%')
			->orderBy('name', 'asc')
			->take(5)
			->get(['id', 'name', 'slug'])
			->toArray();

		$cuisines = Cuisine::where('name','like','%'.$query.'%')
			->has('restaurants')
			->take(5)
			->get(['id', 'name', 'slug'])
			->toArray();

		// Data normalization
		$restaurants = $this->appendURL($restaurants, 'restaurant');
		$cuisines    = $this->appendURL($cuisines, 'cuisine');

		// Add type of data to each item of each set of results
		$restaurants = $this->appendValue($restaurants, 'restaurant', 'class');
		$cuisines    = $this->appendValue($cuisines, 'cuisine', 'class');

		// Merge all data into one array
		$data = array_merge($restaurants, $cuisines);

		return Response::json( ['data' => $data] );
	}


}
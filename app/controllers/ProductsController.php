<?php

class ProductsController extends \BaseController {

	/**
	 * Display a listing of products
	 *
	 * @return Response
	 */
	public function index()
	{
		$products = Product::all();

		return View::make('admin.products.index', compact('products'));
	}

	/**
	 * Show the form for creating a new product
	 *
	 * @param  int  $id Restaurant_id
	 * @return Response
	 */
	public function create($restaurant)
	{
		foreach (Restaurant::find($restaurant->id)->categories as $c)
			$categories[$c->id] = $c->name;

		return View::make('admin.products.create', compact('restaurant', 'categories'));
	}

	/**
	 * Store a newly created product in storage.
	 *
	 * @return Response
	 */
	public function store($restaurant)
	{
		$data = Input::all();
		$data['restaurant_id'] = $restaurant->id;

		$validator = Validator::make($data, Product::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Product::create($data);

		return Redirect::route('admin.restaurants.show', [$restaurant->id]);
	}

	/**
	 * Display the specified product.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$product = Product::findOrFail($id);

		return View::make('admin.products.show', compact('product'));
	}

	/**
	 * Show the form for editing the specified product.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$product = Product::find($id);

		foreach (Restaurant::find($product->restaurant_id)->categories as $c)
			$categories[$c->id] = $c->name;

		return View::make('admin.products.edit', compact('product', 'categories'));
	}

	/**
	 * Update the specified product in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$product = Product::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Product::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$product->update($data);

		return Redirect::route('admin.restaurants.show', [$product->restaurant_id]);
	}

	/**
	 * Remove the specified product from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$product = Product::find($id);
		$restaurantId = $product->restaurant_id;

		Product::destroy($id);

		return Redirect::route('admin.restaurants.show', [$restaurantId]);
	}

}

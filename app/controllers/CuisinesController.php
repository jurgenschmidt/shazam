<?php

class CuisinesController extends \BaseController {

	/**
	 * Display a listing of cuisines
	 *
	 * @return Response
	 */
	public function index()
	{
		$cuisines = Cuisine::all();

		return View::make('admin.cuisines.index', compact('cuisines'));
	}

	/**
	 * Show the form for creating a new cuisine
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('admin.cuisines.create');
	}

	/**
	 * Store a newly created cuisine in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Cuisine::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$data['slug'] = Str::slug(Input::get('name'));

		Cuisine::create($data);

		return Redirect::route('admin.cuisines.index');
	}

	/**
	 * Display the specified cuisine.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$cuisine = Cuisine::findOrFail($id);

		return View::make('admin.cuisines.show', compact('cuisine'));
	}

	/**
	 * Show the form for editing the specified cuisine.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$cuisine = Cuisine::find($id);

		return View::make('admin.cuisines.edit', compact('cuisine'));
	}

	/**
	 * Update the specified cuisine in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$cuisine = Cuisine::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Cuisine::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$data['slug'] = Str::slug(Input::get('name'));

		$cuisine->update($data);

		return Redirect::route('admin.cuisines.index');
	}

	/**
	 * Remove the specified cuisine from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Cuisine::destroy($id);

		return Redirect::route('admin.cuisines.index');
	}

}

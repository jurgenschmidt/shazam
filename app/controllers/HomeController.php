<?php

class HomeController extends BaseController {

	/**
	 * Show homepage.
	 *
	 * @return View
	 */
	public function index()
	{
		$restaurants = Restaurant::orderBy('updated_at', 'desc')->take(3)->get();

		// Show the page
		return View::make('home.index', compact('restaurants'));
	}

	/**
	 * Display a listing of restaurants
	 *
	 * @return Response
	 */
	public function restaurantIndex()
	{
		$restaurants = Restaurant::all();

		return View::make('home.restaurants', compact('restaurants'));
	}

	/**
	 * Display a listing of cuisines
	 *
	 * @return Response
	 */
	public function cuisineIndex()
	{
		$cuisines = Cuisine::all();

		return View::make('home.cuisines', compact('cuisines'));
	}

	/**
	 * View a restaurant.
	 *
	 * @param  string  $slug
	 * @return View
	 */
	public function restaurantView($slug)
	{
		// Get restaurant data
		$restaurant = Restaurant::whereSlug($slug)->first();

		// Get this restaurant categories
		$categories = $restaurant->categories()->get();

		// Show the page
		return View::make('home/restaurant', compact('restaurant', 'categories'));
	}

	/**
	 * View a cuisine.
	 *
	 * @param  string  $slug
	 * @return View
	 */
	public function cuisineView($slug)
	{
		// Get cuisine data
		$cuisine = Cuisine::whereSlug($slug)->first();

		// Get this cuisine restaurants

		// Show the page
		return View::make('home/cuisine', compact('cuisine'));
	}

}

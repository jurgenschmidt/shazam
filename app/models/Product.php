<?php

class Product extends \Eloquent {

	protected $fillable = ['name', 'price', 'description', 'restaurant_id', 'category_id'];

	public static $rules = ['name' => 'required'];

	public function restaurant()
	{
		return $this->belongsTo('Restaurant');
	}

	public function category()
	{
		return $this->belongsTo('Category');
	}

}
<?php

class Restaurant extends \Eloquent {

	protected $fillable = ['name', 'address', 'city', 'about', 'phone', 'delivery', 'longitude', 'latitude', 'slug'];

	public static $rules = [
		'name'    => 'required',
		'address' => 'required',
		'city'    => 'required',
		'about'   => 'required'
	];

	public function cuisines()
	{
		return $this->belongsToMany('Cuisine', 'cuisine_restaurant', 'restaurant_id', 'cuisine_id');
	}

	public function products()
	{
		return $this->hasMany('Product');
	}

	public function categories()
	{
		return $this->hasMany('Category');
	}

}
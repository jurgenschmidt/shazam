<?php

class Cuisine extends \Eloquent {

	protected $fillable = ['name'];

	public static $rules = ['name' => 'required'];

	public function restaurants()
	{
		return $this->belongsToMany('Restaurant', 'cuisine_restaurant', 'cuisine_id', 'restaurant_id');
	}

}
<?php

class Category extends \Eloquent {

	protected $fillable = ['name', 'restaurant_id'];

	public static $rules = ['name' => 'required'];

	public function restaurant()
	{
		return $this->belongsTo('Restaurant');
	}

	public function products()
	{
		return $this->hasMany('Product');
	}

}
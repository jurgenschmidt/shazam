<?php

class CategoriesTableSeeder extends Seeder {

	public function run()
	{
		$foobar = Restaurant::where('name','=','FooBar Delivery')->first()->id;
		$coxinharia = Restaurant::where('name','=','Coxinharia')->first()->id;
		$casadacuca = Restaurant::where('name','=','Casa da cuca')->first()->id;

		Category::create(['name' => 'Lanches', 'restaurant_id' => $foobar]);
		Category::create(['name' => 'Lanches', 'restaurant_id' => $coxinharia]);
		Category::create(['name' => 'Coxinhas', 'restaurant_id' => $coxinharia]);
		Category::create(['name' => 'Bebidas', 'restaurant_id' => $coxinharia]);
		Category::create(['name' => 'Comidas', 'restaurant_id' => $casadacuca]);
	}

}
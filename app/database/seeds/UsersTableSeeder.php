<?php

class UsersTableSeeder extends Seeder {

	public function run()
	{
		$users = [
			[
				'username' => 'yuga',
				'password' => Hash::make('password'),
				'email' => 'yuga@estrela10.com.br'
			]
		];

		foreach ($users as $user) {
			User::create($user);
		}
	}

}
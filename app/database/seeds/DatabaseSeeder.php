<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('CuisinesTableSeeder');
		$this->call('RestaurantsTableSeeder');
		$this->call('CuisineRestaurantTableSeeder');
		$this->call('UsersTableSeeder');
		$this->command->info('The database has been seeded!');
	}

}

<?php

class ProductsTableSeeder extends Seeder {

	public function run()
	{
		$foobar = Restaurant::where('name','=','FooBar Delivery')->first()->id;
		$coxinharia = Restaurant::where('name','=','Coxinharia')->first()->id;
		$casadacuca = Restaurant::where('name','=','Casa da cuca')->first()->id;

		$lanchesFoobar = Category::where(function($query)
			{
				$query->where('name','=','Lanches')
					->where('restaurant_id','=',$foobar);
			})->first()->id;

		$lanchesCoxinharia = Category::where(function($query)
			{
				$query->where('name','=','Lanches')
					->where('restaurant_id','=',$foobar);
			})->first()->id;

		// Category::create(['name' => 'Lanches', 'restaurant_id' => $coxinharia]);
		// Category::create(['name' => 'Coxinhas', 'restaurant_id' => $coxinharia]);
		// Category::create(['name' => 'Bebidas', 'restaurant_id' => $coxinharia]);
		// Category::create(['name' => 'Comidas', 'restaurant_id' => $casadacuca]);

		Product::create([
			'name' => 'Foo',
			'price' => '9.90',
			'description' => 'Foo bar really tasty',
			'restaurant_id' => '1',
			'category_id' => '1'
		]);

		Product::create([
			'name' => 'Coxinha',
			'price' => '2.50',
			'description' => 'coxinha deliciosa',
			'restaurant_id' => '2',
			'category_id' => '2'
		]);

		Product::create([
			'name' => 'Coxinha',
			'price' => '2.50',
			'description' => 'coxinha deliciosa',
			'restaurant_id' => '2',
			'category_id' => '2'
		]);
	}

}
<?php

class CuisinesTableSeeder extends Seeder {

	public function run()
	{
		Cuisine::create(['name' => 'Delivery', 'slug' => 'delivery']);
		Cuisine::create(['name' => 'Lanches', 'slug' => 'lanches']);
		Cuisine::create(['name' => 'Coxinhas', 'slug' => 'coxinhas']);
		Cuisine::create(['name' => 'Cucas', 'slug' => 'cucas']);
	}

}
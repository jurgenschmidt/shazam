<?php

class CuisineRestaurantTableSeeder extends Seeder {

	public function run()
	{
		$delivery = Cuisine::where('name','=','Delivery')->first()->id;
		$lanches = Cuisine::where('name','=','Lanches')->first()->id;
		$coxinhas = Cuisine::where('name','=','Coxinhas')->first()->id;
		$cucas = Cuisine::where('name','=','Cucas')->first()->id;

		$foobar = Restaurant::where('name','=','FooBar Delivery')->first()->id;
		$coxinharia = Restaurant::where('name','=','Coxinharia')->first()->id;
		$casadacuca = Restaurant::where('name','=','Casa da cuca')->first()->id;

		$cuisine_restaurant = [
			// Foobar -> Delivery
			['cuisine_id' => $delivery, 'restaurant_id' => $foobar],

			// Coxinharia -> Delivery
			['cuisine_id' => $delivery, 'restaurant_id' => $coxinharia],

			// Foobar -> Lanches
			['cuisine_id' => $lanches, 'restaurant_id' => $foobar],

			// Coxinharia -> Lanches
			['cuisine_id' => $lanches, 'restaurant_id' => $coxinharia],

			// Coxinharia -> Coxinhas
			['cuisine_id' => $coxinhas, 'restaurant_id' => $coxinharia],

			// Casa da cuca -> Cucas
			['cuisine_id' => $cucas, 'restaurant_id' => $casadacuca]
		];

		DB::table('cuisine_restaurant')->insert($cuisine_restaurant);
	}

}
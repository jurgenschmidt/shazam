<?php

class RestaurantsTableSeeder extends Seeder {

	public function run()
	{
		Restaurant::create([
			'name' => 'FooBar Delivery',
			'address' => 'Rua foobaz',
			'city' => 'Marechal Cândido Rondon',
			'about' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit sunt consequuntur eveniet modi eligendi fugiat libero dolor, aperiam at adipisci, deleniti nisi harum reiciendis molestiae a, tempore quis quam vero.',
			'phone' => '45 1234-5678',
			'delivery' => 'Sim',
			'slug' => 'foobar-delivery'
		]);

		Restaurant::create([
			'name' => 'Coxinharia',
			'address' => 'Rua da coxinha',
			'city' => 'Marechal Cândido Rondon',
			'about' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit sunt consequuntur eveniet modi eligendi fugiat libero dolor, aperiam at adipisci, deleniti nisi harum reiciendis molestiae a, tempore quis quam vero.',
			'phone' => '45 1234-5678',
			'delivery' => 'Sim',
			'slug' => 'coxinharia'
		]);

		Restaurant::create([
			'name' => 'Casa da cuca',
			'address' => 'Rua da cuca',
			'city' => 'Marechal Cândido Rondon',
			'about' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit sunt consequuntur eveniet modi eligendi fugiat libero dolor, aperiam at adipisci, deleniti nisi harum reiciendis molestiae a, tempore quis quam vero.',
			'phone' => '45 1234-5678',
			'delivery' => 'Não',
			'slig' => 'casa-da-cuca'
		]);
	}

}